#include"mypro.h"
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<string.h>
#include<ctype.h>
#include<errno.h>
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAG "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

//DEFAULT GREP MATCHING PATTERN IN LINE AND PRINITING THOSE LINES
char *grep( char *line, char *pattern, int argc){
	char *arr[100];	
	int j = 0;
	if(strstr(line,pattern)){
		arr[j] = line;
		j++;
		return *arr;	
	}
	
	return 0;			
}
//PRINTING THE LINES THAT DO NOT CONTAIN THE PATTERN
char *grepp( char *line, char *pattern, int argc){
	char *arr[100];	
	int j = 0;
	if(!strstr(line,pattern)){
		arr[j] = line;
		j++;
		return *arr;	
	}
	
	return 0;			
}
//PRINTING THE LINES CONTAINING THE PATTERNS IN THE PATTERN FILE
char *grep_ff(char *b, char *i, int argc){
	int k = 0,count = 0,j = 0;
	char buf[10000],*array[100];
	FILE *fd = fopen(i, "r");
	fread(buf,sizeof(buf), 1, fd); 
	char *arr[1000];
	char *r = strtok(buf,"\n");
	while(r != NULL){
		arr[k] = (char *)malloc(sizeof(r));
		arr[k] = r;
		r = strtok(NULL,"\n");
		k++;	
		count++;
	}
	k = 0;
	while(k < count ){
		if((array[j] = grep(b,arr[k],argc)))
		j++;
		k++;	
	}
	
 return *array;
}	
		
//PRINTS THE NUMBER OF MATHCED LINES IN FILE
int grep_cc( char *line, char *pattern, int argc){
	if(strstr(line,pattern)){
		return 1;	
	}
	
	return 0;			
}

//FINDING INSENSITIVE PATTERN IN LINES EX: FROM = from 
void grep_i(char *b, char *i,int argc){
	FILE *fd;
	char buf[10000], buff[10000],pat[200];
	int j = 0;
	fd = fopen(b, "r");
	if(fd == NULL){	
		perror("unable to open");
		return;
	}
 	fread(buf,sizeof(buf), 1, fd);
	char *r = strtok(buf,"\n");
	while( r != NULL){	
		while(r[j]){
			buff[j] = tolower(r[j]);
			j++;
		}
		j = 0;
		while(i[j]){
			pat[j] = tolower(i[j]);
			j++;
		}
		if(strstr(buff,pat)){
			if(argc <= 4){
				printf("%s\n",r);
				j++;
			}
			if(argc > 4){
				printf(MAG"%s"BLUE ":"RESET" %s\n",b,r);
			}
		}
		r = strtok(NULL,"\n");
		
	}

	 fclose(fd);
}



//MATCHES THE WHOLE WORD IN THE LINES
char *w(char *line,char *i,int argc, char *b){
	char *p = (char *)malloc(sizeof(line));
	strcpy(p,line);
	char *word = strtok(line," ");
	while(word != NULL){
		if(strcmp(word,i) == 0){
			return p;
		}
		
		word = strtok(NULL," ");
	}
	return 0;
	
}



//PRINTS THE NUMBER OF LINES = num MATCHED WITH PATTERN
void grep_m(char *b, char *i, int num,int argc){
	char buf[10000];
	int k = 0,count = 0;
	FILE *fd = fopen(b, "r");
	if(fd == NULL){	
		perror("unable to open");
		return;
	}
	fread(buf,sizeof(buf), 1, fd); 
	char *arr[100];
	char *r = strtok(buf,"\n");
	while(r != NULL){
		arr[k] = (char *)malloc(sizeof(r));
		arr[k] = r;
		r = strtok(NULL,"\n");
		k++;	
	}
	k = 0;
	while(arr[k]){
		if(count < num){
			if(strstr(arr[k],i)){
				if(argc == 5){
					printf("%s\n",arr[k]);
					count++;
				}
				if(argc == 6){
					printf(MAG"%s"BLUE ":"GREEN" %s\n"RESET,b,arr[k]);
					count++;
				}
			}
		}
		
	k++;
	}
}

//PRINTS THE WORDS MATCHED WITH THE LINES IN FILES
char *oo(char *b, char *i,char *file,int argc){
	char *word = strtok(b," ");
	while(word != NULL){
		if(strstr(word,i)){
			if(argc <= 4){
					printf("%s\n",word);
				}
				if(argc > 4){
					printf(MAG"%s"BLUE ":"GREEN" %s\n"RESET,file,word);

				}
		}
		
		word = strtok(NULL," ");
	}
return  0;
}


//PRINTS THE LINE NO OF THE PATTERN MATCHED LIMIT = num
void grep_n(char *b, char *i,int argc,int lineno,char *file){
	if(strstr(b,i)){
		if(argc == 4){
			printf(GREEN"%d"BLUE":"RESET"%s",lineno,b);
		}
		if(argc == 5 ){
			printf(MAG"%s"BLUE ":"GREEN"%d"BLUE":"RESET"%s",file,lineno,b);
		}
		if(argc > 6 ){
			printf(MAG"%s"BLUE ":"GREEN"%d"BLUE":"RESET"%s",file,lineno,b);
		}
	}
	
}
//PRINTS FILE NAME AND PATTERN NAME WITH MULTIPLE FILES
char *grep_H(char *b, char *i){
	int j = 0;
	char *arr[100];
	if(strstr(b,i)){
		arr[j] = b;
		j++;
		return *arr;		
	}
	return 0;	

}

//PRINT THE LINES MATCHED WITH THE PATTERN FROM TWO FILES
char *grep_h(char *b , char *i){
	int j = 0;
	char *arr[100];
	if(strstr(b,i)){
		arr[j] = b;
		j++;
		return *arr;		
	}
	return 0;		
}
//COMBINATION OF -v & -f
char *grep_fv(char *b, char *i, int argc){
	int k = 0,count = 0,j = 0;
	char buf[10000],*array[100];
	FILE *fd = fopen(i, "r");
	if(fd == NULL){	
		perror("unable to open");
		return 0;
	}
	fread(buf,sizeof(buf), 1, fd); 
	char *arr[1000];
	char *r = strtok(buf,"\n");
	while(r != NULL){
		arr[k] = (char *)malloc(sizeof(r));
		arr[k] = r;
		r = strtok(NULL,"\n");
		k++;	
		count++;
	}
	k = 0;
	while(k < count ){
		if((array[j] = grepp(b,arr[k],argc)))
		j++;
		k++;	
	}
	
 return *array;
}
//COMBINATION OF -v & -c
int grep_vc( char *line, char *pattern, int argc){
	if(!strstr(line,pattern)){
		return 1;	
	}
	
	return 0;			
}
//COMBINATION OF -vnm
char *grep_vnm(char *b, char *i,int argc,int lineno,char *file){	
	char *arr;
	if(!strstr(b,i)){
		arr = b;	
		return arr;
	}	
	return 0;	
}
//COMBINATION OF -vn
void grep_vn(char *b, char *i,int argc,int lineno,char *file){
	if(!strstr(b,i)){
		if(argc <= 4){
			printf(GREEN"%d"BLUE":"RESET"%s",lineno,b);
		}
		if(argc > 4){
			printf(MAG"%s"BLUE ":"GREEN"%d"BLUE":"RESET"%s",file,lineno,b);
		}
	}
	
}

