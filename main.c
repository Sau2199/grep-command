#include<stdio.h>
#include"mypro.h"
#include<string.h>
#include<errno.h>
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAG "\x1b[35m"
#define CYAN "\x1b[36m"
#define RESET "\x1b[0m"

int main(int argc , char *argv[]){
	char *i,*opt;
	int j = 2;
	opt = argv[1];
	i = argv[argc-1];
	//DEFAULT GREP MATCHING THE PATTERN WITH LINES
	if(strcmp(opt,"grep") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP...SYNTAX:[grep DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep( buf,i, argc);
					if(line){
						
						if(argc == 4){
							printf("%s",line);
						}
						if(argc == 5){
							printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);
						} 
						
					
					}
			}
		j++;	
		}
	}
	//PRINTING THE LINES THAT DO NO MATCH WITH THE PATTERN
	if(strcmp(opt,"-v") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_V...SYNTAX:[-v DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grepp( buf,i, argc);
					if(line){
						if(argc == 4){
							printf("%s",line);
						}
						if(argc == 5){
							printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);
						} 
						
					
					}
			}
		j++;	
		}
	}
	//PRINT THE COUNT OF LINES MATCHED WITH THE PATTERN
	if(strcmp(opt,"-c") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_C...SYNTAX:[-c DATAFILES PATTERN]\n");
		}
		if(argc > 5){
				printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int count = 0;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				count = count + grep_cc( buf,i, argc);		
			}
			if(argc == 4){
				printf("%d\n",count);
			}
			if(argc == 5){
				printf(MAG"%s"BLUE ":"RESET"%d\n",argv[j],count);
			}
			
		j++;	
		}
	}
	// PRINTS THE LINES CONTAINING THE WHOLE WORD 
	if(strcmp(opt,"-w") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_w...SYNTAX:[-w DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = w( buf,i, argc,argv[j]);
				if(line){
						if(argc == 4){
							printf("%s",line);
						}
						if(argc == 5){
							printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);
						} 
				}
			}
		j++;	
		}
	}
	//PRINT THE LINES THAT MATCHES THE PATTERN IN A FILE
	if(strcmp(opt,"-f") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_f...SYNTAX:[-f DATAFILES PATTERN_FILE]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
			goto end;
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}		
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_ff( buf,i, argc);
				if(line){
					if(argc == 4){
						printf("%s",line);
					}
					if(argc == 5){
						printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);
					} 
					
				}
			}
		j++;	
		}
	}
	end:
	//PRINTS THE WORD THAT MATCHES THE PATTERN IN LINE
	if(strcmp(opt,"-o") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_O...SYNTAX:[-o DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				
				char *line = oo(buf,i,argv[j],argc);
					if(line){
						if(argc == 4){
							printf("%s",line);
						}
						if(argc == 5){
							printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);
						} 
						
					
					}
			}
		j++;	
		}
	}
	//PRINT THE LINES IN FILE MATCHING THE PATTERN
	if(strcmp(opt,"-h") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_h...SYNTAX:[-h DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_h( buf,i);
				if(line)
					printf("%s",line);	
			}
		j++;	
		}
	}
	//PRINTS THE LINES WITH THE FILENAME MATCHING THE PATTERN
	if(strcmp(opt,"-H") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_H...SYNTAX:[-H DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_h( buf,i);
				if(line)
					printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);	
			}
		j++;	
		}
	}
	//PRINTS THE LINES MATCHING WITH UPPERCASE AND LOWERCASE
	if(strcmp(opt,"-i") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_i...SYNTAX:[-i DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			grep_i(argv[j], i, argc);
			j++;
		}
	}

	//PRINTS THE LINES WITH THE LIMIT OF MAXCOUNT
	if(strcmp(opt,"-m") == 0 ){
		if(argc < 5){
			printf("Please Enter Valid Command: GREP_m...SYNTAX:[-m DATAFILES PATTERN maxcount]\n");
		}
		if(argc > 6){
			printf("Please Enter Valid Command\n");
		}
		int num = atoi(argv[argc-1]);
		i = argv[argc-2];
		while(j < (argc-2)){
			grep_m(argv[j],i,num,argc);
			j++;
		}
	}
	//PRINTS THE LINE NUMBER OF THE LINE THAT MATCHES WITH THE PATTERN
	if(strcmp(opt,"-n") == 0 ){		
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_n...SYNTAX:[-n DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int lineno = 1;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){                   	
				grep_n(buf,i,argc,lineno,argv[j]);
				lineno++;
			}
		j++;	
		}
	}
	//COMBINATION OF -v & -h
	if(strcmp(opt,"-vh") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vh...SYNTAX:[-vh DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		char *line;
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grepp( buf,i, argc);
				if(line){
					printf("%s",line);
				}		
			}
		j++;	
		}
		
	}
	
	//COMBINATION OF -v & -H
	if(strcmp(opt,"-vH") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vH...SYNTAX:[-vH DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		
		char *line;

		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}			
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grepp( buf,i, argc);
				if(line){
					printf(MAG"%s"BLUE":"RESET"%s",argv[j],line);
				}		
			}
		j++;	
		}
		
	}
	//COMBINATION OF -v & -c
	if(strcmp(opt,"-vc") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vc...SYNTAX:[-vc DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		char *line;
		while(j < (argc-1)){
			int count = 0,k = 2;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grepp( buf,i, argc);				
				if(line){
					count++;
				}		
			}
			k = j;
			while(k <= j){
				if(argc <= 4){
					printf("%d\n",count);	
				}
				if(argc > 4){
					printf(MAG"%s"BLUE ":"RESET"%d\n"RESET,argv[k],count);
				} 
			k++;		
			}
		j++;	
		}
			
	}
	//COMBINATION OF -c & -h 
	if(strcmp(opt,"-ch") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_ch...SYNTAX:[-ch DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int count = 0;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_h( buf,i);
				if(line)
				count = count + grep_cc( buf,i, argc);			
			}
		printf("%d\n",count);
		j++;	
		}
		
	}
	//COMBINATION OF -c & -H
	if(strcmp(opt,"-cH") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_cH...SYNTAX:[-cH DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int count = 0;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_H( buf,i);
				if(line)
				count = count + grep_cc( buf,i, argc);			
			}
		printf(MAG"%s"BLUE":"RESET"%d\n",argv[j],count);
		j++;	
		}
		
	}
	//COMBINATION OF -c-f
	if(strcmp(opt,"-cf") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_cf...SYNTAX:[-cf DATAFILES PATTERN_FILE]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int count = 0;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_ff( buf,i, argc);
					if(line)
						count++;	
			}
		printf("%d\n",count);
		j++;	
		}
	}
	//COMBINATION OF -v & -f
	if(strcmp(opt,"-vf") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vf...SYNTAX:[-vf DATAFILES PATTERN_FILE]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				char *line = grep_fv( buf,i, argc);
					if(line){
						if(argc == 4){
							printf("%s",line);
						}
						if(argc == 5){
							printf(MAG"%s"BLUE ":"RESET" %s",argv[j],line);
						} 
					
					}
			}
		j++;	
		}
	}
	//COMBINATION OF -vch
	if(strcmp(opt,"-vch") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vch...SYNTAX:[-vch DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		char *line;
		while(j < (argc-1)){
			int count = 0,k = 2;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grepp( buf,i, argc);
				if(line){
					count++;
				}		
			}
			k = j;
			while(k <= j){
				printf("%d\n",count);	
			k++;		
			}
		j++;	
		}
			
	}
	//COMBINATION OF -vn
	if(strcmp(opt,"-vn") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vn...SYNTAX:[-vn DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int lineno = 1;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				grep_vn(buf,i,argc,lineno,argv[j]);	
				lineno++;			
			}
		j++;	
		}
			
	}
	//COMBINATION OF -vnm
	if(strcmp(opt,"-vnm") == 0 ){
		if(argc < 5){
			printf("Please Enter Valid Command: GREP_vnm...SYNTAX:[-vnm DATAFILES PATTERN]\n");
		}
		if(argc > 6){
			printf("Please Enter Valid Command\n");
		}
		i = argv[argc-2];
		
		char *line;
		while(j < (argc-2)){
			int lim = atoi(argv[argc-1]);
			int lineno = 1;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grep_vnm(buf,i,argc,lineno,argv[j]);
				if(line){
					if(lim != 0){				
						if(argc == 5){
							printf(GREEN"%d"BLUE":"RESET"%s",lineno,line);
						}
						if(argc > 5){
							printf(MAG"%s"BLUE ":"GREEN"%d"BLUE":"RESET"%s",argv[j],lineno,line);
						}
					}
				}
					
			lim--;
			lineno++;			
			}
		j++;	
		}
			
	}
	
	//COMBINATION OF -vcH
	if(strcmp(opt,"-vcH") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vcH...SYNTAX:[-vcH DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		char *line;
		while(j < (argc-1)){
			int count = 0,k = 2;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grepp( buf,i, argc);
				if(line){
					count++;
				}		
			}
			k = j;
			while(k <= j){
				printf(MAG"%s"BLUE ":"RESET"%d\n"RESET,argv[k],count); 
			k++;		
			}
		j++;	
		}
			
	}
	//COMBINATION  OF -cnh
	if(strcmp(opt,"-cnh") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_cnh...SYNTAX:[-cnh DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int count = 0;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				count = count + grep_cc( buf,i, argc);		
			}
			printf("%d\n",count);
		j++;	
		}
	}
	//COMBINATION OF -cnH
	if(strcmp(opt,"-cnH") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_cnH...SYNTAX:[-cnH DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		while(j < (argc-1)){
			int count = 0;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				count = count + grep_cc( buf,i, argc);		
			}
				printf(MAG"%s"BLUE ":"RESET"%d\n",argv[j],count);
		j++;	
		}
	}
	//COMBINATION OF -vcn
	if(strcmp(opt,"-vcn") == 0 ){
		if(argc < 4){
			printf("Please Enter Valid Command: GREP_vc...SYNTAX:[-vc DATAFILES PATTERN]\n");
		}
		if(argc > 5){
			printf("Please Enter Valid Command\n");
		}
		char *line;
		while(j < (argc-1)){
			int count = 0,k = 2;
			FILE *fd = fopen(argv[j], "r");
			if(fd == NULL){
				perror("unable to open");
				return errno;
			}
			char buf[10000];
			while(fgets(buf,100,fd)){
				line = grepp( buf,i, argc);				
				if(line){
					count++;
				}		
			}
			k = j;
			while(k <= j){
				if(argc <= 4){
					printf("%d\n",count);	
				}
				if(argc > 4){
					printf(MAG"%s"BLUE ":"RESET"%d\n"RESET,argv[k],count);
				} 
			k++;		
			}
		j++;	
		}
			
	}
	return 0;	
}
