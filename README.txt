TITLE:  GREP-COMMAND

NAME: Saurabh Ajit Patil

MIS ID : 141803011

WORKING:

SYNTAX:./project [OPTION] DATAFILE1 DATAFILE2... PATTERN/PATTERN_FILE  

REFERENCE: Man Page of Grep 
As i have choosen GREP as my mini project ,I started collecting information regarding the various functions of grep.I read the working  
of the functions and started my work.
	My GREP-COMMAND project includes 3 files i.e "mypro.h" "mypro.c" & "main.c". The Program execution starts in main.c it includes various 'if' conditions. The 'if' block executes depending on the argv[1] i.e the option( -i,-v,-c,-n,....,etc). Then the next argument contains the datafile names which contains the data on which our various functions of grep are executed. Then the next argument contains the pattern/pattern_file which is compared with the data in the datafile according the functions the ouptut is printed on the terminal. An Extra argument is required for the 'm' function which contains the maxcount. I have performed error handling in case if filename provided is not present, or in the case if the commands do not match with the syntax of command. I have provided the combinations of options which contain output of one function as the input to the next function.I have attached the required files.

